# Video Streaming Demo

### Things todo list:

1. Clone this repository: `git clone https://gitlab.com/hendisantika/video-streaming-demo.git`
2. Navigate to the folder: `cd video-streaming-demo`
3. Run the application: `mvn clean spring-boot:run`
4. Open index.html file on frontend folder with your favorite browser

### Image Screenshot

Home Page

![Home Page](img/home.png "Home Page")