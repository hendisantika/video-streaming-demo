package com.hendisantika.service;

import com.hendisantika.domain.Video;
import com.hendisantika.repository.VideoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by IntelliJ IDEA.
 * Project : video-streaming-demo
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/8/22
 * Time: 19:21
 * To change this template use File | Settings | File Templates.
 */
class VideoServiceTest {
    VideoRepository videoRepository = mock(VideoRepository.class);
    VideoService videoService = new VideoService(videoRepository);
    String testName = "myVid";

    @Test
    void getVideo() {
        Video expected = new Video(testName, null);
        // When our VideoService object calls repo.findByName(testName), return expected
        when(videoRepository.findByName(testName))
                .thenReturn(expected);
        // When our VideoService object calls videoRepository.existsByName(testName), return true
        when(videoRepository.existsByName(testName))
                .thenReturn(true);
        Video actual = videoService.getVideo(testName);
        assertEquals(expected, actual);
        verify(videoRepository, times(1)).existsByName(testName);
        verify(videoRepository, times(1)).findByName(testName);
    }

    @Test
    void getAllVideoNames() {
        List<String> expected = List.of("myVid", "otherVid");
        when(videoRepository.getAllEntryNames())
                .thenReturn(expected);
        List<String> actual = videoService.getAllVideoNames();
        assertEquals(expected, actual);
        verify(videoRepository, times(1)).getAllEntryNames();
    }

    @Test
    void saveVideo() throws IOException {
        MultipartFile file = mock(MultipartFile.class);
        Video testVid = new Video(testName, file.getBytes());
        videoService.saveVideo(file, testName);
        verify(videoRepository, times(1)).existsByName(testName);
        verify(videoRepository, times(1)).save(testVid);
    }
}