package com.hendisantika.service;

import com.hendisantika.domain.Video;
import com.hendisantika.repository.VideoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * Created by IntelliJ IDEA.
 * Project : video-streaming-demo
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/8/22
 * Time: 19:26
 * To change this template use File | Settings | File Templates.
 */
@SpringBootTest
@Transactional
public class VideoServiceIntegrationTest {
    @Autowired
    VideoService videoService;

    @Autowired
    VideoRepository videoRepository;

    String testName = "myVid";

    @Test
    void getVideo() {
        Video expected = new Video(testName, null);
        videoRepository.save(expected);
        Video actual = videoService.getVideo(testName);
        // The result from service.getVideo(testName) should be expected Video instance above
        assertEquals(expected, actual);
    }

    @Test
    void saveVideo() throws IOException {
        MultipartFile file = mock(MultipartFile.class);
        videoService.saveVideo(file, testName);
        // After saving the video using the service, the repository should say that the video exists
        assertTrue(videoRepository.existsByName(testName));
    }

    @Test
    void getAllVideoNames() {
        List<String> expected = List.of(testName);
        videoRepository.save(new Video(testName, null));
        List<String> actual = videoService.getAllVideoNames();
        // Check the service returns a list of the same contents as the expected list of videos
        assertTrue(expected.size() == actual.size() && expected.containsAll(actual) && actual.containsAll(expected));
    }
}
