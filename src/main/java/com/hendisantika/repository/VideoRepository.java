package com.hendisantika.repository;

import com.hendisantika.domain.Video;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : video-streaming-demo
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/8/22
 * Time: 19:14
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface VideoRepository extends JpaRepository<Video, Long> {
    Video findByName(String name);

    boolean existsByName(String name);

    @Query(nativeQuery = true, value = "SELECT name FROM video")
    List<String> getAllEntryNames();
}
