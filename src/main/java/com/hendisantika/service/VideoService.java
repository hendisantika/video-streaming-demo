package com.hendisantika.service;

import com.hendisantika.domain.Video;
import com.hendisantika.exception.VideoAlreadyExistsException;
import com.hendisantika.exception.VideoNotFoundException;
import com.hendisantika.repository.VideoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : video-streaming-demo
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/8/22
 * Time: 19:16
 * To change this template use File | Settings | File Templates.
 */
@Service
@RequiredArgsConstructor
public class VideoService {
    private final VideoRepository videoRepository;

    public Video getVideo(String name) {
        if (!videoRepository.existsByName(name)) {
            throw new VideoNotFoundException();
        }
        return videoRepository.findByName(name);
    }

    public List<String> getAllVideoNames() {
        return videoRepository.getAllEntryNames();
    }

    public void saveVideo(MultipartFile file, String name) throws IOException {
        if (videoRepository.existsByName(name)) {
            throw new VideoAlreadyExistsException();
        }
        Video newVid = new Video(name, file.getBytes());
        videoRepository.save(newVid);
    }

}
